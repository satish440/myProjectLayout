import { Component,OnInit } from '@angular/core';
import {AppService } from './app.service';
import { AuthenService } from './authen/authen.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isLoggedIn :boolean;
  activeView='productList'; 

  constructor(private service: AppService,private authService:AuthenService) { 

  }

  ngOnInit()
  {
    this.service.viewChangeEvent.subscribe(Pname=>{
      this.activeView=Pname;
    });
    // this.isLoggedIn=this.authService.isLoggedIn;
    this.authService.loginChangeEvent.subscribe(isLoggedIn =>{
      this.isLoggedIn=isLoggedIn;
    });
  }
  onLogOut()
  {
    this.authService.islogOut();
  }

  setDis(Pname){
    this.activeView=Pname;
  }
}
