import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductListItemComponent } from './components/product-list-item/product-list-item.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { AppRoutingModule } from '../app-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  declarations: [ProductListComponent, ProductListItemComponent, ProductDetailComponent],
  exports:[
    ProductListComponent,
    ProductDetailComponent,
    ProductListItemComponent
  ]
})
export class ProductModule { }
