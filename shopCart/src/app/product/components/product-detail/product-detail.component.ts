import { Component, OnInit } from '@angular/core';
import{ActivatedRoute} from '@angular/router';
import{ProductServiceService} from '../../productService';
import IProduct from '../../../Models/product';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})

export class ProductDetailComponent implements OnInit {
  productDetail:IProduct;

  constructor(private route: ActivatedRoute,private service:ProductServiceService) { }

  ngOnInit() {
    const productId=this.route.snapshot.paramMap.get('productId');
    this.service.getProducts().subscribe((products:IProduct[])=>
      {
        this.productDetail=products.find(p=>p.name===productId);
      });

    // this.route.paramMap.subscribe(param =>{
    //   console.log(param.get('productId'));
    // })
  }

}
