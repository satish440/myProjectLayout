import { Component, OnInit } from '@angular/core';
import { ProductServiceService } from '../../productService';
import { Observable } from '../../../../../node_modules/rxjs';
import IProduct from '../../../Models/product';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products=[];
  products$:Observable<IProduct[]>;
  
  constructor(public pdt:ProductServiceService) { }


  ngOnInit() {
        this.products$=this.pdt.getProducts();

}
}

