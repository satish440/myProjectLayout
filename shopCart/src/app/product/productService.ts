import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import{map} from 'rxjs/operators';
import IProduct from '../Models/product';
@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {


  constructor(private http:HttpClient) { }

  getProducts():Observable<IProduct[]>{
    return this.http.get<IProduct[]>('./assets/data/products.json');
  }
  getProductByName(
    productId:string ): Observable<IProduct>
    {
      return this.http.get<IProduct[]>('./assets/data/products.json')
      .pipe(
        map((products:IProduct[])=>{
        return products.find((p:IProduct)=> p.name ===productId)
        })
      )
    }
}
