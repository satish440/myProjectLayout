import { Component, OnInit } from '@angular/core';
import{NgForm} from '@angular/forms';
import { AuthenService } from '../../authen.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 

  constructor(private service :AuthenService) { }

  ngOnInit() {
    
  }
  onLoginSubmit(myForm:NgForm)
  {
    if(myForm.valid)
    {
      console.log(myForm.value);
    }
    else{
      alert("Invalid Form");
    }
  }

}
