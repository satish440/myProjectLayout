import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MinicartComponent } from './components/minicart/minicart.component';
import { CartListComponent } from './components/cart-list/cart-list.component';
import { CheckoutComponent } from './components/checkout/checkout.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MinicartComponent, CartListComponent, CheckoutComponent],
  exports:[
    MinicartComponent,
    CartListComponent
  ]
})
export class CartModule { }
