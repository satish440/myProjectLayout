export default interface IProduct
{
    name:string;
    model:string;
    company:string;
    description:string;
    imgUrl:string;
}