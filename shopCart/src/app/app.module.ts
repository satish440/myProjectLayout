import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{ AppRoutingModule } from './app-routing.module';
import{ HttpClientModule } from '@angular/common/http';
import {ProductModule} from './product/product.module';
import{CartModule} from './cart/cart.module';
import { AppComponent } from './app.component';
import { AuthenModule } from './authen/authen.module';




@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ProductModule,
    CartModule, 
    AuthenModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
