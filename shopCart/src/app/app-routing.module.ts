import { NgModule } from '@angular/core';
import{ RouterModule, Routes } from '@angular/router';
import {ProductListComponent} from './product/components/product-list/product-list.component';
import {ProductDetailComponent} from './product/components/product-detail/product-detail.component';
import {CartListComponent} from './cart/components/cart-list/cart-list.component';
import{LoginComponent} from './authen/components/login/login.component';
const routes:Routes=[
  {
    path:'login',
    component:LoginComponent
    },
  {
  path:'products',
  component:ProductListComponent
  },

  {
    path:'cart',
    component:CartListComponent
    },
    {
      path:'product-detail/:productId',
      component:ProductDetailComponent
      },
      
  {
    path:'',
    redirectTo:'/products',
    pathMatch:'full'
    },
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    
  ],
  exports:[
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
