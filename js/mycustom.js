$(document).ready(function (){
    var userName =signerName();
    $('#logIn').on('click',function() {
    	if(userlogin() == true)
        {
            window.location.href ='Dashboard.html';
        }    
        return false;
    });

    $('#logOut').on('click',function(){
        // sessionStorage.clear();
        window.location.href = 'index.html';
        return false;
    });
    $('#reGister').on('click',function() {
        window.location.href = 'Register.html';
        return false;
    });
    $('#user_data').on('click',function() {
        window.location.href = 'index.html';
        return true;
    });

    $(document).on('click','#user_data',function(){
     	dataSave();
    });

    function signerName(){
        var sName =sessionStorage.getItem("lastname");
        $('#logger').append('<strong>'+sName+'</strong>');
    }

    function dataSave(){
        var values = {} ,data;
        $('.form-control').each(function(){
           var iid = $(this).attr("id");
            var val=$(this).val();
            values[iid] = val;           
        });
        var flag = false;
        if(localStorage.getItem('udata') == null ){
            data = [] ;
            data.push(values);   
            localStorage.setItem('udata',JSON.stringify(data)); 
        } else{
            data = JSON.parse(localStorage.getItem('udata'));
            for(var i in data){
                if(data[i].email == values["email"]){
                    flag = true;
                }
            }
            flag == false ? data.push(values) : alert("Email Already Exits");
            localStorage.setItem('udata',JSON.stringify(data));
        }            
    }

    function userlogin(){
    	var Mydata=(JSON.parse(localStorage.getItem('udata')));
    	var email = $("#User").val() ,i = 0;
    	var pWd = $("#Pword").val();
    	for(i = 0 ; i < Mydata.length ; i++){
            if( (Mydata[i].uname == email  || Mydata[i].email == email ) && Mydata[i].rpwd == pWd){
                sessionStorage.setItem("lastname", email);
                alert("Successfully Login");
                 return true;
            }

        }
        if(i == Mydata.length){
            window.location.href = 'index.html';
            alert("Credintilas are not matched");
            return false;
        }
    }

    $(function(){
        $('.form-control').keyup(function(){
             if ($("#uname").val() == '') {
                $('regbtn').attr('disabled', true);

            }
            if ($("#email").val() == '') {
                $('regbtn').attr('disabled', true);
            }
            if ($("#rpwd").val() == '') {
                $('regbtn').attr('disabled', true);
            }
            if ($("#cfmpwd").val() == '') {
                $('regbtn').attr('disabled', true);

            }
            if ($("#mobile").val() == '') {
                $('regbtn').attr('disabled', true);

            }           
            else
                $('.regbtn').attr('disabled',false);
        })
    });

    /*Validations*/
    $(document).on(" focus, blur", ".form-control", function(e) {
        if ($(this).val() == "") {
            $(this).addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Field is Required..?");
            
        } 
    });

    function required(a) {
        if (a.val() == "") {
            a.addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Dont leave this feild");
            return false;

        } else {
            return true;
        }
    }

    function username(a) {
        var pattern = /^[a-zA-Z]*$/;
        var fname = a.val();
        if (pattern.test(fname) && fname.length>5 && fname !== '') {
            a.removeClass("is-invalid").addClass("is-valid").next().removeClass("invalid-feedback").addClass("valid-feedback").text("Your Name is Valid...!");
           
        } 
            else {
            a.addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Should contain more than 5 characters");
          
        }
    }

    function email(a) {
        var pattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var uemail = $("#email").val()
        if (pattern.test(uemail) && uemail !== '') {
            a.removeClass("is-invalid").addClass("is-valid").next().removeClass("invalid-feedback").addClass("valid-feedback").text("You Enetred Correct Mail Id!");
          
        } else {
            a.addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Should contain One Special Char and Uppercase Char");
           
        }
    }

    function passWord(a) {

        var passwd = $("#rpwd").val().length;
        if (passwd < 8) {
             a.addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Password Should Contain 8 letters!!!!");
           
        } else {
             a.removeClass("is-invalid").addClass("is-valid").next().removeClass("invalid-feedback").addClass("valid-feedback").text("You Enetred Password is valid !!!");
          
        }
    }

    function confPwd(a) {

        var passwd = $("#rpwd").val();
        var cfm_pwd = $("#cfmpwd").val();
        if (passwd !== cfm_pwd) {
             a.addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Password Didn't Matched!!!!");
           
        } else {
             a.removeClass("is-invalid").addClass("is-valid").next().removeClass("invalid-feedback").addClass("valid-feedback").text("You Enetred Password is Matched !!!");
            
        }
    }


    function Mobile(a) {
        var pattern = /[0-9 -()+]+$/;
        var num = a.val();
        if (pattern.test(num) && num !== '') {
            if (num.length == 10) {
                a.addClass("is-valid").removeClass("is-invalid").next().addClass("valid-feedback").removeClass("invalid-feedback").text("You entered  Mobile is valid..");
               
            } else {
                a.addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Mobile No is Must be 10 Numbers");
            }

        } else {
            a.removeClass("is-valid").addClass("is-invalid").next().removeClass("valid-feedback").addClass("invalid-feedback").text("You Enetred Mobile No is InValid!");
        }
    }

    $(".form-control").on("input", function() {

            if (required($(this))) {

                $(this).attr("id") == "uname" ? username($(this)) : "";
                $(this).attr("id") == "email" ? email($(this)) : "";
                 $(this).attr("id") == "rpwd" ? passWord($(this)) : "";
                  $(this).attr("id") == "cfmpwd" ? confPwd($(this)) : "";
                $(this).attr("id") == "mobile" ? Mobile($(this)) : "";

            }
    });
     /*validations end*/



    var  filters=[];
    $('input[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
           var ip=$(this).val();
            filters.push(ip);
        }
        else if($(this).is(":not(:checked)")){
            if( filters.indexOf($(this).val()) >= 0){
                 filters.splice(filters.indexOf($(this).val()) , 1);
            }
        }
        $(".getf").empty();
        $(".secimg").empty();
        console.log( filters);
        for (i=0;i< filters.length;i++){
            var template ='<div class = "btnCus text-white text-center col-6">'+filters[i]+'<i class="far fa-times-circle pl-2"></i></div>';
            $(".getf").append(template);

            var  temp='<div class="col-lg-3">'+
                        '<div class="card border-success mb-3">'+
                            '<div class="card-body text-success">'+
                              '<img src="images/'+filters[i]+'.jpg" class="img-fluid w-100" style="width:100px; height: 100px;">'+
                       '</div>'+
                       '<div class="card-footer bg-transparent border-success ">'+filters[i]+'<i class="fas fa-download float-right"></i>'+'  </div>'+
                     '</div>'
            $(".secimg").append(temp);
        } 
    });
    $(document).on('click' , '.btnCus i' , function(){
        console.log($(this).parent().text());
        if( filters.indexOf($(this).parent().text()) >= 0){
            filters.splice(filters.indexOf($(this).parent().text()) , 1);
        }
        var xyz = $.trim($(this).parent().text());
        $('input[type="checkbox"]').each(function(){
            if(this.checked == true){
                if(this.value == xyz){
                    this.checked = false;
                }
            }
        });
        $(".getf").empty();
        $(".secimg").empty();
        console.log( filters);
        for (i=0;i< filters.length;i++){
            var template = '<div class = "btnCus text-white text-center col-6">'+filters[i]+'<i class="far fa-times-circle pl-2"></i></div>';
            $(".getf").append(template);
            var  temp='<div class="col-lg-3 imgShow">'+
                     '<div class="card border-success mb-3">'+
                         '<div class="card-body text-success">'+
                             '<img src="images/'+filters[i]+'.jpg" class="img-fluid w-100" style="width:100px; height: 100px;">'+
                         '</div>'+
                         '<div class="card-footer bg-transparent border-success ">'+filters[i]+'<i class="fas fa-download float-right"></i>'+ '</div>'+
                     '</div>'
            $(".secimg").append(temp);                     
        }
    }); 

  var data=($("#cate_data").text());
  
    localStorage.setItem("a",data);
      var gdata= JSON.parse(localStorage.getItem("a"));
      console.log(gdata);
      for(var x in gdata.categories){

         for (var i=0;i< gdata.categories[x].length;i++){
            var  dimg='<div class="col-lg-3 imgShow">'+
                     '<div class="card border-success mb-3">'+
                         '<div class="card-body text-success">'+
                             '<img src="'+gdata.categories[x][i].ipath+'" class="img-fluid w-100" style="width:100px; height: 100px;">'+
                         '</div>'+ 
                          '<div class="card-footer bg-transparent border-success ">'+gdata.categories[x][i].id+'<i class="fas fa-download float-right"></i>'+ '</div>'+        
                     '</div>';
            $(".secimg").append(dimg);                     
        }
      }
      

    $('#myInput').on('keyup',function(){
        var value = $(this).val().toLowerCase();
        $(".imgShow").filter(function(index) {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
 });
