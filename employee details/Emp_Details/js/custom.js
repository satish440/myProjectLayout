$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "http://localhost:4040/notes",
        cache: false,
        success: function (response) {
            console.log(response[0].Name);
            $(document).on("click", '#detail', function () {
                 var template = '<div class="row bg-info text-dark"><div class="col-lg-2"><h3>Name</h3></div><div class="col-lg-2"><h3>Position</h3></div><div class="col-lg-2"><h3>Office</h3></div><div class="col-lg-2"><h3>Age</h3></div><div class="col-lg-2"><h3>StartDate</h3></div><div class="col-lg-2"><h3>Salary</h3></div></div>';
                    $(".get").append(template);
                var data = [];
                for (var i = 0; i < response.length; i++) {
                    var value = '<div class="row emp bg-light py-2 my-2 text-dark id="emp"><div class="col-lg-2">' + response[i].Name + '</div><div class="col-lg-2">' + response[i].position + '</div><div class="col-lg-2">' + response[i].office + '</div><div class="col-lg-2">' + response[i].Age + '</div><div class="col-lg-2">' + response[i].StartDate + '</div><div class="col-lg-2">' + response[i].Salary + '</div></div>';
                    $('#table_data').append(value);
                }
                 $('.selCt').on('change', function () {
                $("#table_data").empty();
                for (var i = 0; i < $('.selCt :selected').val(); i++) {
                   var value = '<div class="row emp bg-light py-2 my-2 text-dark id="emp"><div class="col-lg-2">' + response[i].Name + '</div><div class="col-lg-2">' + response[i].position + '</div><div class="col-lg-2">' + response[i].office + '</div><div class="col-lg-2">' + response[i].Age + '</div><div class="col-lg-2">' + response[i].StartDate + '</div><div class="col-lg-2">' + response[i].Salary + '</div></div>';
                    $('#table_data').append(value);
                }
            });
            });
        }
    });
     $('#myInput').on('keyup',function(){
        var value = $(this).val().toLowerCase();
        $(".emp").filter(function(index) {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
