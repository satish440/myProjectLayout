const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    Name: String,
    position: String,
    office: String,
    Age: Number,
    StartDate: String,
    Salary:Number
});

module.exports = mongoose.model('Note', NoteSchema);