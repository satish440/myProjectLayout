import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import{AppRoutingModule} from './app-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';





@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    RegisterComponent,
    LoginComponent,
   
   
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
