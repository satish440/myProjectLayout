import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Route} from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { RegisterComponent } from './register/register.component';
import{LoginComponent} from './login/login.component';
let Route= [
{
  path:'main',
  component:LayoutComponent
},
{
  path:'reg',
  component:RegisterComponent
},
{
  path:'',
  // redirectTo:'login',
  // pathMatch:'full'
  component:LoginComponent
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(Route)

  ],
  exports:[
    RouterModule,
  
  ],
  declarations: []
})
export class AppRoutingModule { }
